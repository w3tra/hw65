import React from 'react';
import './Menu.css';
import {NavLink} from "react-router-dom";

const Menu = () => {
  return(
    <div className="Menu">
      <ul className="Menu-list">
        <li><NavLink to="/">Home</NavLink></li>
        <li><NavLink to="/about" >About</NavLink></li>
        <li><NavLink to="/contacts" >Contacts</NavLink></li>
        <li><NavLink to="/news" >News</NavLink></li>
        <li><NavLink to="/admin/edit">Admin</NavLink></li>
      </ul>
    </div>
  )
};

export default Menu;