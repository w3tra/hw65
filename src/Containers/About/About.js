import React, { Component } from 'react';
import axios from 'axios';
import Spinner from "../../Components/Spinner/Spinner";

class About extends Component {

  state = {
    data: [],
    loading: false
  };

  getPageInfo = () => {
    let url = 'about';
    if (this.props.location.pathname.split('/')[1]) {
      url = this.props.location.pathname.split('/')[1]
    }
    this.setState({loading: true});
    axios.get(url + '.json').then(response => {
      const pageInfo = ({id: new Date(), title: response.data.title, text: response.data.text});
      this.setState({data: pageInfo});
    }).then(() => this.setState({loading: false}));
  };

  componentDidMount() {
    this.getPageInfo();
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.page !== prevProps.match.params.page) {
      this.setState({loading: true});
      const page = this.props.match.params.page;
      if (page) {
        axios.get(`/${page}.json`).then(response => {
          const pageInfo = ({id: new Date(), title: response.data.title, text: response.data.text});
          this.setState({data: pageInfo});
        }).then(() => this.setState({loading: false}));
      } else {
        this.getPageInfo();
      }
    }
  }

  render() {
    let page =
      <div className="About-container">
        <h2>{this.state.data.title}</h2>
        <p>{this.state.data.text}</p>
      </div>;

    if (this.state.loading) {
      page = <Spinner />;
    }
    return page;
  }

}

export default About;