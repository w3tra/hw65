import React, {Component} from 'react';
import {Button, FormControl, Input, InputLabel, MenuItem, Select, TextField} from "@material-ui/core";
import './Admin.css';
import axios from "axios/index";
import Spinner from "../../Components/Spinner/Spinner";

class Admin extends Component {

  state = {
    page: '',
    title: '',
    text: '',
    loading: false
  };

  changeHandler = event => this.setState({[event.target.name]: event.target.value });

  handleChange = event => this.setState({page: event.target.value});

  editpageHandler = () => {
    this.setState({loading: true});
    let object = {title: this.state.title, text: this.state.text};
    axios.patch(`/${this.state.page}.json`, object).then(() => {
      this.setState({loading: false});
      this.props.history.push(`/${this.state.page}`);
    });
  };

  componentDidUpdate(props, prevState) {
    if (prevState.page !== this.state.page) {
      this.setState({loading: true});
      axios.get(`/${this.state.page}.json`).then(response => {
        this.setState({title: response.data.title, text: response.data.text});
      }).then(() => this.setState({loading: false}));
    }
  }

  render() {
    let form = (
      <div className="Admin-container">
        <h2>Редактирование страницы</h2>
        <div className="select-form">
          <FormControl>
            <InputLabel>Категория</InputLabel>
            <Select value={this.state.page} onChange={this.handleChange} className="Admin-select">
              <MenuItem value="about">About</MenuItem>
              <MenuItem value="contacts">Contacts</MenuItem>
              <MenuItem value="news">News</MenuItem>
            </Select>
          </FormControl>
        </div>
        <div className="input-form">
          <FormControl>
            <Input className="Admin-input" name="title"
                   value={this.state.title} onChange={this.changeHandler} />
          </FormControl>
        </div>
        <div className="text-form">
          <TextField className="Admin-text" multiline rows="10" name="text"
                     value={this.state.text} onChange={this.changeHandler}/>
        </div>
        <Button variant="contained" className="btn" onClick={this.editpageHandler}>Save</Button>
      </div>
    );
    if (this.state.loading) {
      form = <Spinner/>;
    }
    return form;
  }
}

export default Admin;